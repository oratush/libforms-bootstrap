<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\helpers
 * @file
 *  ErrorMessageHelper.php
 *
 * Defines helper methods for printing error messages.
 */

namespace com\oratush\forms\helpers;
use com\oratush\forms\FormError as FormError;

class ErrorMessageHelper
{
    /**
     * Prepares error message to be displayed in a bootstrap alert div.
     *
     * @access public
     *
     * @param $errors array containing FormErrors which will be outputted
     *
     * @return string HTML formatted
     */
    public static function prepareMessage($errors)
    {
        $total = count($errors);

        $error_message = '<strong>Please correct the following:</strong><br />';

        for ($i = 0; $i < $total; $i++) {
            $error = $errors[$i];

            // Initialize useful variables;
            $error_code = $error->getCode();

            $label = null;
            if ($error->getControl() != null) {
                $label = $error->getControl()->getLabel();
            }
            $field_label = !empty($label) ?
                '<br />The field <strong>"' . substr($label, 0, -1) . '"</strong> ' :
                '';

            $file_related_error_txt = !empty($label) ?
                '<br />The file selected for <strong>"' . substr($label, 0, -1) . '"</strong> ' :
                '';

            // Add proper message per code.
            switch ($error_code) {
                case FormError::ERROR_NO_VALUE:
                    $error_message .= $field_label . 'is required.';
                    break;
                case FormError::ERROR_NO_SELECTION:
                    if (!empty($field_label)) {
                        $error_message .= $field_label . 'must have a selected value.';
                    } else {
                        $error_message .= '<br />You must select one of the possible values.';
                    }
                    break;
                case FormError::ERROR_NO_GROUP_VALUE:
                    $error_message .= '<br />You must provide value to at least one field.';
                    break;
                case FormError::ERROR_MATCHING_PASSWORDS:
                    $fields = $error->getControls();

                    $error_message .= '<br />The passwords you provided in the fields: <strong>';
                    $error_message .= substr($fields[0]->getLabel(), 0, -1);
                    $error_message .= ", " . substr($fields[1]->getLabel(), 0, -1);
                    $error_message .= '</strong>, do not match.';
                    break;
                case FormError::ERROR_INVALID_LIST:
                    $error_message .= $field_label . '"does not contain valid values.';
                    break;
                case FormError::ERROR_MULTIPLE_SELECTIONS:
                case FormError::ERROR_INVALID_SELECTIONS:
                    if (!empty($field_label)) {
                        $error_message .= $field_label . 'does not have valid selected items.';
                    } else {
                        $error_message .= '<br />The field does not have valid selected items.';
                    }
                    break;
                case FormError::ERROR_HAS_VALUE:
                    $error_message .= $field_label . 'must not have a value.';
                    break;
                case FormError::ERROR_HAS_SELECTION:
                    if (!empty($field_label)) {
                        $error_message .= $field_label . 'must not have a selected value.';
                    } else {
                        $error_message .= '<br />You must not select any value.';
                    }
                    break;
                case FormError::ERROR_DB_BAD_CREDENTIALS:
                    $error_message .= '<br />The username and password you provided are incorrect.';
                    break;
                case FormError::ERROR_DB_ENTRY_EXISTS:
                    $error_message .= $field_label . 'value already exists in the database.';
                    break;
                case FormError::ERROR_INVALID_FLOAT:
                    $error_message .= $field_label . 'must contain a valid float.';
                    break;
                case FormError::ERROR_INVALID_NUMBER:
                    $error_message .= $field_label . 'must contain a valid integer.';
                    break;
                case FormError::ERROR_INVALID_EMAIL:
                    $error_message .= $field_label . 'must contain a valid email.';
                    break;
                case FormError::ERROR_LENGTH_TOO_SMALL:
                    $error_message .= $field_label . 'has length smaller than required.';
                    break;
                case FormError::ERROR_LENGTH_OVERFLOW:
                    $error_message .= $field_label . 'has length bigger than required.';
                    break;
                case FormError::ERROR_RANGE_TOO_LOW:
                    $error_message .= $field_label . 'is outside the min range.';
                    break;
                case FormError::ERROR_RANGE_TOO_HIGH:
                    $error_message .= $field_label . 'is outside the max range.';
                    break;
                case FormError::ERROR_INVALID_DATE:
                    $error_message .= $field_label . 'does not contain a valid date/time.';
                    break;
                case FormError::ERROR_INVALID_ISO_DATETIME:
                    $error_message .= $field_label . 'does not contain a valid ISO date and time.';
                    break;
                case FormError::ERROR_INVALID_ISO_DATE:
                    $error_message .= $field_label . 'does not contain a valid ISO date.';
                    break;
                case FormError::ERROR_INVALID_ISO_TIME:
                    $error_message .= $field_label . 'does not contain a valid ISO time.';;
                    break;
                case FormError::ERROR_MATCHING_REGEX:
                    $error_message .= $field_label . 'is not valid.';
                    break;
                case FormError::ERROR_INVALID_CAPTCHA:
                    $error_message .= '<br />The words you entered for the CAPTCHA challenge where incorrect.';
                    break;
                case FormError::ERROR_FILE_UPLOAD_ERROR:
                    $error_message .= $file_related_error_txt . 'failed to upload.';
                    break;
                case FormError::ERROR_FILE_BAD_FORM_TYPE:
                    $error_message .= $file_related_error_txt . 'could not be located.';
                    break;
                case FormError::ERROR_FILE_NOT_SELECTED:
                    $error_message .= $file_related_error_txt . 'was empty.';
                    break;
                case FormError::ERROR_FILE_SELECTED:
                    $error_message .= $file_related_error_txt . 'was not empty.';
                    break;
                case FormError::ERROR_FILE_MAX_SIZE_LIMIT:
                case FormError::ERROR_FILE_MAX_SIZE_LIMIT_WITH_PAYLOAD:
                    $error_message .= $file_related_error_txt . 'was too big.';
                    break;
                case FormError::ERROR_FILE_MIME_TYPE:
                    $error_message .= $file_related_error_txt . 'did not contain a valid MIME header.';
                    break;
                case FormError::ERROR_FILE_EXTENSION:
                    $error_message .= $file_related_error_txt . 'had a bad extension.';
                    break;
                case FormError::ERROR_FILE_BAD_IMAGE_DIMENSIONS:
                    $error_message .= $file_related_error_txt . 'does not have the correct dimensions.';
                    break;
                case FormError::ERROR_FILE_BAD_IMAGE:
                    $error_message .= $file_related_error_txt . 'is not a valid image.';
                    break;
                default:
                    $error_message .= '<br />Unknown error code: ' . $error_code . ' (' . $error->getMessage() . ').';
            }
        }

        return $error_message;
    }
}
