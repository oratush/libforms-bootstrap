<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\helpers
 * @file
 *  BootstrapButtonHelper.php
 *
 * Defines helper methods for returning json inputs for button elements.
 */

namespace com\oratush\forms\helpers;

class BootstrapButtonHelper
{
    /**
     * Prepares and returns a JSON representation of an OK BUTTON SET for bootstrap.
     *
     * @access public
     *
     * @param $ok_label string with the button label.
     * @return string JSON representation of an OK BUTTON SET for bootstrap.
     */
    public static function getOkButton($ok_label)
    {
        $json_string = <<<EOT
"ButtonSet": {
  "buttons": [
   {
    "name": "ok_btn",
    "type": "submit",
    "classes": [
      "btn-primary"
     ],
    "value": "ok",
    "label": "$ok_label"
   }
  ]
 }
EOT;

        return $json_string;
    }

    /**
     * Prepares and returns a JSON representation of an OK, Cancel BUTTON SET for bootstrap.
     *
     * @param $ok_label string with the button label.
     * @param $cancel_label string with the cancel label.
     * @return string JSON representation of an OK, Cancel BUTTON SET for bootstrap.
     */
    public static function getOkCancelButton($ok_label, $cancel_label)
    {
        $json_string = <<<EOT
"ButtonSet": {
  "buttons": [
   {
    "name": "ok_btn",
    "type": "submit",
    "classes": [
      "btn-primary"
     ],
    "value": "ok",
    "label": "$ok_label"
   },
   {
    "name": "cancel_btn",
    "type": "reset",
    "classes": [
      "btn-default"
     ],
    "value": "cancel",
    "attributes": {
      "style": "margin-left: 10px"
     },
    "label": "$cancel_label"
   }
  ]
 }
EOT;

        return $json_string;
    }

    /**
     * Prepares and returns a JSON representation of an OK, Cancel BUTTON SET for bootstrap. The cancel
     * button is not a reset button, but a link to navigate the user in case cancel is pressed/touched.
     *
     * @param $ok_label string with the button label.
     * @param $cancel_label string with the cancel label.
     * @param $cancel_link string with the url of the cancel link.
     * @return string JSON representation of an OK, Cancel BUTTON SET for bootstrap.
     */
    function getOkCancelButtonLink($ok_label, $cancel_label, $cancel_link)
    {
        $json_string = <<<EOT
"ButtonSet": {
  "buttons": [
   {
    "name": "ok_btn",
    "type": "submit",
    "classes": [
      "btn-primary"
     ],
    "value": "ok",
    "label": "$ok_label"
   },
   {
    "type": "link",
    "classes": [
      "btn-default"
     ],
    "link": "$cancel_link",
    "attributes": {
      "style": "margin-left: 10px"
     },
    "label": "$cancel_label"
   }
  ]
 }
EOT;

        return $json_string;
    }
}
