<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\helpers
 * @file
 *  BootstrapFormHelper.php
 *
 * Defines helper methods for returning json inputs for complete forms.
 */

namespace com\oratush\forms\helpers;

class BootstrapFormHelper
{
    /**
     * Prepares and returns a standard login form in JSON format.
     *
     * @return string JSON representation of a bootstrap (v2) login form.
     */
    function loginFormV2()
    {
        $buttons = BootstrapButtonHelper::getOkButton("Login");

        $json_string = <<<EOT
{
 "id": "login_frm",
 "name": "login_frm",
 "controls": [
   {
    "Fieldset": {
     "legend": "Sign in",
     "controls": [
      {
       "InputText": {
         "id": "input01",
         "name": "username",
         "classes": [
           "input-xlarge"
          ],
         "label": "Username:",
         "autofocus": true,
         "placeholder": "Your username"
        }
      },
      {
       "InputPassword": {
         "id": "input02",
         "name": "password",
         "classes": [
           "input-xlarge"
          ],
         "label": "Password:",
         "placeholder": "Your password"
        }
      },
      {
       $buttons
      }
     ]
    }
   }
  ],
 "validators": [
   {
    "SingleFieldRequired": {
      "fields": [
       "username",
       "password"
      ]
     }
   },
   {
    "CheckCredentials": {
      "usr": "username",
      "pwd": "password",
      "dbc": "db_conn",
      "query": "SELECT user_id, password FROM users WHERE enabled=1 AND username=?"
     }
   }
  ]
}
EOT;

        return $json_string;
    }

    /**
     * Prepares and returns a standard login form in JSON format.
     *
     * @return string JSON representation of a bootstrap (v3) login form.
     */
    function loginFormV3()
    {
        $buttons = BootstrapButtonHelper::getOkButton("Login");

        $json_string = <<<EOT
{
 "id": "login_frm",
 "name": "login_frm",
 "controls": [
   {
    "Fieldset": {
     "legend": "Sign in",
     "controls": [
      {
       "InputText": {
         "id": "input01",
         "name": "username",
         "label": "Username:",
         "autofocus": true,
         "placeholder": "Your username"
        }
      },
      {
       "InputPassword": {
         "id": "input02",
         "name": "password",
         "label": "Password:",
         "placeholder": "Your password"
        }
      },
      {
       $buttons
      }
     ]
    }
   }
  ],
 "validators": [
   {
    "SingleFieldRequired": {
      "fields": [
       "username",
       "password"
      ]
     }
   },
   {
    "CheckCredentials": {
      "usr": "username",
      "pwd": "password",
      "dbc": "db_conn",
      "query": "SELECT user_id, password FROM users WHERE enabled=1 AND username=?"
     }
   }
  ]
}
EOT;

        return $json_string;
    }
}
