<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\output
 * @file
 *  Bootstrap3Horizontal.php
 *
 * Class BootstrapHorizontal defines the object responsible for drawing
 * the error message and the elements of a form in Bootstrap (v3)
 * horizontal form.
 */

namespace com\oratush\forms\output;
use com\oratush\forms\controls\AbstractControl as AbstractControl;

class Bootstrap3Horizontal extends BootstrapHorizontal
{
    private $grid_label;
    private $grid_input;

    public function __construct()
    {
        parent::__construct();

        $this->grid_label = 2;
        $this->grid_input = 4;
    }

    public function setGridLayout($label, $input)
    {
        $this->grid_label = $label;
        $this->grid_input = $input;
    }

    protected function drawClasses($classes)
    {
        if ($classes != null) {
            $total = count($classes);
            $prints = 0;
            for ($i = 0; $i < $total; $i++) {

                if (mb_strpos($classes[$i], "col-sm-") === 0) {
                    continue;
                }

                if ($prints != 0) {
                    echo(' ');
                }
                echo($classes[$i]);
                $prints++;
            }
        }
    }

    protected function drawInputText($control)
    {
        $this->drawControlGroupWithLabel($control);

        echo('  <input type="' . $control->getType() . '" id="' . $control->getID(
          ) . '" name="' . $control->getName() . '" class="form-control');

        $this->drawCommonWithoutClass($control);
        $this->drawInputTextCommon($control);
    }

    protected function drawInputCheckbox($control)
    {
        $this->drawControlGroup();

        $in_grid = $this->getGridInput($control);

        $value = $control->getLabel();
        if ($value != null) {
            echo(' <label class="control-label col-sm-' . $this->grid_label . '">' . $this->escapeHTML(
                $value
            ) . '</label>');

            echo(' <div class="col-sm-' . $in_grid . '">');
        } else {
            echo(' <div class="col-sm-offset-' . $this->grid_label . ' col-sm-' . $in_grid . '">');
        }

        $elements = $control->getElements();
        $total = count($elements);
        for ($i = 0; $i < $total; $i++) {
            if ($control->isMultiline()) {
                echo('  <div class="' . $control->getType() . '">');
            }

            echo('<label');
            if (!$control->isMultiline()) {
                echo(' class="' . $control->getType() . '-inline"');
            }
            echo('>');

            echo('  <input type="' . $control->getType() . '" name="' . $control->getName());
            if ($control->getType() == AbstractControl::INPUT_CHECKBOX) {
                echo('[]');
            }
            echo('"');

            $this->drawCommon($control);

            //From INPUT
            if ($control->isReadonly()) {
                echo(' readonly="readonly"');
            }
            //--

            $value = $elements[$i]->getValue();
            if ($value != null) {
                echo(' value="' . $value . '"');
            }

            if ($elements[$i]->isDisabled()) {
                echo(' disabled="disabled"');
            }
            if ($elements[$i]->isSelected()) {
                echo(' checked="checked"');
            }

            echo(' />');

            echo($this->escapeHTML($elements[$i]->getLabel()));

            echo('</label>');

            if ($control->isMultiline()) {
                echo('  </div>');
            }
        }

        $this->drawControlEnd($control);
    }

    protected function drawButtons($buttons)
    {
        $this->drawControlGroup();

        //echo('<div class="form-group" style="
        //background-color: #F5F5F5; border-top: 1px solid #E5E5E5; padding-top: 20px; padding-bottom: 20px; margin-bottom: 20px; margin-top: 20px;">');
        echo(' <div class="col-sm-offset-' . $this->grid_label . ' col-sm-4">');

        $this->drawButtonsCommon($buttons);

        echo(' </div>');
        echo('</div>');
    }
    
    protected function drawTextarea($control)
    {
        $this->drawControlGroupWithLabel($control);

        echo('  <textarea id="' . $control->getID() . '" name="' . $control->getName(
          ) . '" class="form-control');

        $this->drawCommonWithoutClass($control);
        $this->drawTextareaCommon($control);
    }
    
    protected function drawSelect($control)
    {
        $this->drawControlGroupWithLabel($control);

        echo('  <select id="' . $control->getID() . '" name="' . $control->getName());
        if ($control->isMultiple()) {
            echo('[]');
        }

        echo('" class="form-control');

        $this->drawCommonWithoutClass($control);
        $this->drawSelectCommon($control);
    }

    private function getGridInput($control)
    {
        $final_grid = $this->grid_input;

        $classes = $control->getClasses();
        if ($classes != null) {
            $total = count($classes);
            for ($i = 0; $i < $total; $i++) {
                if (mb_strpos($classes[$i], "col-sm-") === 0) {
                    $final_grid = mb_substr($classes[$i], 7);
                    break;
                }
            }
        }

        return $final_grid;
    }

    private function drawCommonWithoutClass($control)
    {
        $classes = $control->getClasses();
        if ($classes != null) {
            echo(' ');
            $this->drawClasses($classes);
        }
        echo('"');

        $this->drawAttributes($control->getAttributes());

        if ($control->isDisabled()) {
            echo(' disabled="disabled"');
        }
        if ($control->isAutofocus()) {
            echo(' autofocus="autofocus"');
        }
    }

    protected function drawControlGroup()
    {
        echo('<div class="form-group">');
    }

    protected function drawControlGroupWithLabel($control)
    {
        $this->drawControlGroup();
        echo(' <label class="control-label col-sm-' . $this->grid_label . '" for="' . $control->getID(
        ) . '">' . $this->escapeHTML($control->getLabel()) . '</label>');

        $in_grid = $this->getGridInput($control);
        echo(' <div class="col-sm-' . $in_grid . '">');
    }

    protected function getErrorClass()
    {
        return "alert alert-danger";
    }
}
