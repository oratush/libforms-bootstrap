<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\output
 * @file
 *  BootstrapHorizontal.php
 *
 * Class BootstrapHorizontal defines the object responsible for drawing
 * the error message and the elements of a form in Bootstrap (v2)
 * horizontal form.
 */

namespace com\oratush\forms\output;
use com\oratush\forms\controls\AbstractControl as AbstractControl;
use com\oratush\forms\controls\Button as Button;

class BootstrapHorizontal
{
    private $form;

    public function __construct()
    {
        $this->form = null;
    }

    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * This methods outputs the Bootstrap (version 2) form using the
     * provided form object.
     */
    public function draw()
    {
        //Stop if we get bad input
        if ($this->form == null || !is_a($this->form, "com\\oratush\\forms\\Form")) {
            return;
        }

        $value = $this->form->getTranslatedErrorMessage();
        if ($value != null && strlen($value) > 0) {
            echo('<div class="'. $this->getErrorClass() .'">' . $value . '</div>');
        }

        echo('<form class="form-horizontal ');

        $this->drawClasses($this->form->getClasses());

        echo('" method="' . $this->form->getMethod());
        echo('" name="' . $this->form->getName());
        echo('" action="' . $this->form->getAction());
        echo('" enctype="' . $this->form->getEncodingType());

        $value = $this->form->getID();
        if ($value != null) {
            echo('" id="' . $value);
        }
        $value = $this->form->getAcceptCharset();
        if ($value != null) {
            echo('" accept-charset="' . $value);
        }
        $value = $this->form->getTarget();
        if ($value != null) {
            echo('" target="' . $value);
        }
        if (!$this->form->isValidate()) {
            echo('" novalidate="novalidate');
        }
        if ($this->form->isAutocomplete()) {
            echo('" autocomplete="on"');
        }
        else {
            echo('" autocomplete="off"');
        }

        $this->drawAttributes($this->form->getAttributes());

        echo('>');
        //Hidden field to assist script in order to identify the submitted form
        echo('<input type="hidden" value="1" name="' . $this->form->getName() . '_form_check" />');

        $items = $this->form->getControls();
        $total = count($items);
        for ($i = 0; $i < $total; $i++) {
            $item = $items[$i];

            if ($item->getControlType() == AbstractControl::FIELDSET) {
                echo('<fieldset');
                if ($item->isDisabled()) {
                    echo(' disabled="disabled"');
                }

                $value = $item->getID();
                if ($value != null) {
                    echo(' id="' . $value . '"');
                }
                echo('>');

                $value = $item->getLegend();
                if ($value != null) {
                    echo('<legend>' . $this->escapeHTML($value) . '</legend>');
                }

                $elements = $item->getControls();
                $elemtotal = count($elements);
                for ($j = 0; $j < $elemtotal; $j++) {
                    $this->drawControl($elements[$j]);
                }

                echo('</fieldset>');
            } else {
                $this->drawControl($item);
            }
        }

        echo('</form>');
    }

    private function drawControl($control)
    {
        if ($control->getControlType() == AbstractControl::INPUT) {
            if ($control->getType() == AbstractControl::INPUT_TEXT
                    || $control->getType() == AbstractControl::INPUT_PASSWORD) {
                $this->drawInputText($control);
            } else if ($control->getType() == AbstractControl::INPUT_HIDDEN) {
                echo('  <input type="' . $control->getType() . '" id="' . $control->getID(
                ) . '" name="' . $control->getName() . '"');

                $value = $control->getValue();
                if ($value != null) {
                    echo(' value="' . $this->escapeHTML($value) . '"');
                }

                echo(' />');
            } else if ($control->getType() == AbstractControl::INPUT_FILE) {
                $this->drawInputFile($control);
            } else if ($control->getType() == AbstractControl::INPUT_IMAGE) {
                $this->drawInputImage($control);
            } else if ($control->getType() == AbstractControl::INPUT_CHECKBOX
                    || $control->getType() == AbstractControl::INPUT_RADIO) {
                $this->drawInputCheckbox($control);
            }
        } else if ($control->getControlType() == AbstractControl::BUTTON
                || $control->getControlType() == AbstractControl::BUTTONSET) {
            $buttons = array();
            if ($control->getControlType() == AbstractControl::BUTTON) {
                $buttons[0] = $control;
            } else {
                $buttons = $control->getButtons();
            }

            $this->drawButtons($buttons);
        } else if ($control->getControlType() == AbstractControl::TEXTAREA) {
            $this->drawTextarea($control);
        } else if ($control->getControlType() == AbstractControl::SELECT) {
            $this->drawSelect($control);
        } else if ($control->getControlType() == AbstractControl::CAPTCHA) {
            $this->drawCaptcha($control);
        }
    }

    //Private methods

    private function drawInputFile($control)
    {
        $this->drawControlGroupWithLabel($control);

        echo('  <input type="' . $control->getType() . '" id="' . $control->getID(
        ) . '" name="' . $control->getName());
        if ($control->isMultiple() && substr($control->getName(), strlen($control->getName()) - strlen("[]")) != "[]") {
            echo('[]');
        }
        echo('"');

        $this->drawCommon($control);

        //From INPUT
        if ($control->isReadonly()) {
            echo(' readonly="readonly"');
        }
        //--

        if ($control->isMultiple()) {
            echo(' multiple="multiple"');
        }
        $value = $control->getAccept();
        if ($value != null) {
            echo(' accept="' . $value . '"');
        }

        echo(' />');

        $this->drawControlEnd($control);
    }

    private function drawInputImage($control)
    {
        $this->drawControlGroupWithLabel($control);

        echo('  <input type="' . $control->getType() . '" id="' . $control->getID(
          ) . '" name="' . $control->getName() . '"');

        $this->drawCommon($control);

        //From INPUT
        if ($control->isReadonly()) {
            echo(' readonly="readonly"');
        }
        //--

        $value = $control->getWidth();
        if ($value != null) {
            echo(' width="' . $value . '"');
        }
        $value = $control->getHeight();
        if ($value != null) {
            echo(' height="' . $value . '"');
        }
        $value = $control->getSource();
        if ($value != null) {
            echo(' src="' . $value . '"');
        }
        $value = $control->getAlt();
        if ($value != null) {
            echo(' alt="' . $value . '"');
        }

        echo(' />');

        $this->drawControlEnd($control);
    }

    private function drawCaptcha($control)
    {
        $this->drawControlGroupWithLabel($control);

        if ( $control->getVersion()==1 ) {
            $use_ssl = false;
            if( isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
                $use_ssl = true;
            }

            echo(recaptcha_get_html($control->getPublic(), null, $use_ssl));
        }
        else
        {
            echo('<div class="g-recaptcha" data-sitekey="'. $control->getPublic() .'"></div>');
        }

        $this->drawControlEnd($control);
    }

    //Shared functions

    protected function drawInputText($control)
    {
        $this->drawControlGroupWithLabel($control);

        echo('  <input type="' . $control->getType() . '" id="' . $control->getID(
        ) . '" name="' . $control->getName() . '"');

        $this->drawCommon($control);
        $this->drawInputTextCommon($control);
    }

    protected function drawInputTextCommon($control) {
        //From INPUT
        if ($control->isReadonly()) {
            echo(' readonly="readonly"');
        }

        if ($control->getType() != AbstractControl::INPUT_PASSWORD) {
            $value = $control->getValue();
            if ($value != null) {
                echo(' value="' . $this->escapeHTML($value) . '"');
            }
        }

        //From InputText
        $value = $control->getPlaceholder();
        if ($value != null) {
            echo(' placeholder="' . $this->escapeHTML($value) . '"');
        }
        $value = $control->getSize();
        if ($value != null) {
            echo(' size="' . $value . '"');
        }
        $value = $control->getMaxlength();
        if ($value != null) {
            echo(' maxlength="' . $value . '"');
        }
        if ($control->isAutocomplete()) {
            echo(' autocomplete="on"');
        } else {
            echo(' autocomplete="off"');
        }
        //--

        echo(' />');

        $this->drawControlEnd($control);
    }

    protected function drawInputCheckbox($control)
    {
        $this->drawControlGroup();

        $value = $control->getLabel();
        if ($value != null) {
            echo(' <label class="control-label">' . $this->escapeHTML($value) . '</label>');
        }

        echo(' <div class="controls">');

        $elements = $control->getElements();
        $total = count($elements);
        for ($i = 0; $i < $total; $i++) {
            echo('<label class="' . $control->getType());
            if (!$control->isMultiline()) {
                echo(' inline');
            }
            echo('">');

            echo('  <input type="' . $control->getType() . '" name="' . $control->getName());
            if ($control->getType() == AbstractControl::INPUT_CHECKBOX) {
                echo('[]');
            }
            echo('"');

            $this->drawCommon($control);

            //From INPUT
            if ($control->isReadonly()) {
                echo(' readonly="readonly"');
            }
            //--

            $value = $elements[$i]->getValue();
            if ($value != null) {
                echo(' value="' . $value . '"');
            }

            if ($elements[$i]->isDisabled()) {
                echo(' disabled="disabled"');
            }
            if ($elements[$i]->isSelected()) {
                echo(' checked="checked"');
            }

            echo(' />');

            echo($this->escapeHTML($elements[$i]->getLabel()));

            echo('</label>');
        }

        $this->drawControlEnd($control);
    }

    protected function drawButtons($buttons)
    {
        echo('<div class="form-actions">');

        $this->drawButtonsCommon($buttons);

        echo('</div>');
    }

    protected function drawButtonsCommon($buttons)
    {
        $total = count($buttons);
        for ($i = 0; $i < $total; $i++) {
            if ($buttons[$i]->getType() == Button::TYPE_LINK) {
                echo('<a href="' . $buttons[$i]->getLink() . '" class="btn ');

                $this->drawClasses($buttons[$i]->getClasses());
                echo('"');

                $this->drawAttributes($buttons[$i]->getAttributes());

                if ($buttons[$i]->isDisabled()) {
                    echo(' disabled="disabled"');
                }

                $value = $buttons[$i]->getID();
                if ($value != null) {
                    echo(' id="' . $value . '"');
                }

                echo('>' . $this->escapeHTML($buttons[$i]->getLabel()) . '</a>');
            } else {
                echo('<button type="' . $buttons[$i]->getType() . '" class="btn ');

                $this->drawClasses($buttons[$i]->getClasses());
                echo('"');

                $this->drawAttributes($buttons[$i]->getAttributes());

                if ($buttons[$i]->isDisabled()) {
                    echo(' disabled="disabled"');
                }
                if ($buttons[$i]->isAutofocus()) {
                    echo(' autofocus="autofocus"');
                }

                $value = $buttons[$i]->getID();
                if ($value != null) {
                    echo(' id="' . $value . '"');
                }
                $value = $buttons[$i]->getName();
                if ($value != null) {
                    echo(' name="' . $value . '"');
                }
                $value = $buttons[$i]->getValue();
                if ($value != null) {
                    echo(' value="' . $value . '"');
                }

                echo('>' . $this->escapeHTML($buttons[$i]->getLabel()) . '</button>');
            }
        }
    }

    protected function drawTextarea($control)
    {
        $this->drawControlGroupWithLabel($control);

        echo('  <textarea id="' . $control->getID() . '" name="' . $control->getName() . '"');

        $this->drawCommon($control);
        $this->drawTextareaCommon($control);
    }

    protected function drawTextareaCommon($control)
    {
        //From TextArea
        if ($control->isReadonly()) {
            echo(' readonly="readonly"');
        }

        $value = $control->getPlaceholder();
        if ($value != null) {
            echo(' placeholder="' . $this->escapeHTML($value) . '"');
        }

        $value = $control->getColumns();
        if ($value != null) {
            echo(' cols="' . $value . '"');
        }
        $value = $control->getRows();
        if ($value != null) {
            echo(' rows="' . $value . '"');
        }
        $value = $control->getMaxlength();
        if ($value != null) {
            echo(' maxlength="' . $value . '"');
        }
        $value = $control->getWrap();
        if ($value != null) {
            echo(' wrap="' . $value . '"');
        }

        echo('>');

        $value = $control->getValue();
        if ($value != null) {
            echo($this->escapeHTML($value));
        }

        echo('</textarea>');

        $this->drawControlEnd($control);
    }

    protected function drawSelect($control)
    {
        $this->drawControlGroupWithLabel($control);

        echo('  <select id="' . $control->getID() . '" name="' . $control->getName());
        if ($control->isMultiple()) {
            echo('[]');
        }
        echo('"');

        $this->drawCommon($control);
        $this->drawSelectCommon($control);
    }

    protected function drawSelectCommon($control)
    {
        //From Select
        $value = $control->getSize();
        if ($value != null) {
            echo(' size="' . $value . '"');
        }
        if ($control->isMultiple()) {
            echo(' multiple="multiple"');
        }

        echo('>');

        $this->drawSelectElements($control->getElements());

        echo('</select>');

        $this->drawControlEnd($control);
    }

    /** Common attributes */
    protected function drawCommon($control)
    {
        $classes = $control->getClasses();
        if ($classes != null) {
            echo(' class="');
            $this->drawClasses($classes);
            echo('"');
        }

        $this->drawAttributes($control->getAttributes());

        if ($control->isDisabled()) {
            echo(' disabled="disabled"');
        }
        if ($control->isAutofocus()) {
            echo(' autofocus="autofocus"');
        }
    }

    protected function drawControlGroup()
    {
        echo('<div class="control-group">');
    }

    /** Draw the control group with the label */
    protected function drawControlGroupWithLabel($control)
    {
        $this->drawControlGroup();
        echo('<label class="control-label" for="' . $control->getID() . '">'
            . $this->escapeHTML($control->getLabel()) . '</label>');
        echo(' <div class="controls">');
    }

    /** Draw the helptext (if any) of the control element and the closure divs */
    protected function drawControlEnd($control)
    {
        $value = $control->getHelptext();
        if ($value != null) {
            echo('<span class="help-block">' . $value . '</span>');
        }

        echo(' </div>');
        echo('</div>');
    }

    /** The classes to use for the error message */
    protected function getErrorClass()
    {
        return "alert alert-error";
    }

    /** Draw the items inside a Select element */
    private function drawSelectElements($elems)
    {
        $total = count($elems);
        for ($i = 0; $i < $total; $i++) {
            $item = $elems[$i];

            if ($item->getControlType() == AbstractControl::GROUP) {
                echo('<optgroup label="' . $this->escapeHTML($item->getLabel()) . '"');
                if ($item->isDisabled()) {
                    echo(' disabled="disabled"');
                }
                echo('>');

                $this->drawSelectElements($item->getElements());

                echo('</optgroup>');
            } else {
                echo('<option value="' . $item->getValue() . '"');
                if ($item->isDisabled()) {
                    echo(' disabled="disabled"');
                }
                if ($item->isSelected()) {
                    echo(' selected="selected"');
                }
                echo('>' . $this->escapeHTML($item->getLabel()) . '</option>');
            }
        }
    }

    /** Draw additional classes that the element may have */
    protected function drawClasses($classes)
    {
        if ($classes != null) {
            $total = count($classes);
            for ($i = 0; $i < $total; $i++) {
                if ($i != 0) {
                    echo(' ');
                }
                echo($classes[$i]);
            }
        }
    }

    /** Draw additional attributes that the element may have */
    protected function drawAttributes($attributes)
    {
        if ($attributes != null) {
            $keys = array_keys($attributes);
            $total = count($keys);

            for ($i = 0; $i < $total; $i++) {
                echo(' ' . $keys[$i] . '="' . $attributes[$keys[$i]] . '"');
            }
        }
    }

    protected function escapeHTML($value)
    {
        return htmlentities($value);
    }
}
