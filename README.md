# LIBFORMS-BOOTSTRAP #

Output reference implementation to draw a libforms form using bootstrap. This is a sister project of libforms: A PHP HTML forms library with server side validation.

For complete documentation please visit the [libforms site](http://libforms.oratush.com)